gulp = require 'gulp'
gutil = require 'gulp-util'
sass = require 'gulp-ruby-sass'
using = require 'gulp-using'
jade = require './custom_jade.js'
gulpif = require 'gulp-if'
debug = require 'gulp-debug'
concat = require 'gulp-concat'
rename = require 'gulp-rename'
through = require 'through2'
browserSync = require 'browser-sync'
highlight = require 'pygments'
jaden = require 'jade'





src = 'src'
target = 'www'

handleError = (err) ->
  console.log(err.toString())
  this.emit('end')

gulp.task 'styles', ->
  gulp.src("#{ src }/styles/main.scss")
      .pipe(sass(style: 'expanded', debugInfo: true))
      .on("error", handleError)
      .pipe(gulpif(true, gulp.dest("#{ target }/styles")))
      .pipe(using(prefix: 'Output CSS to'))

gulp.task 'scripts', ->
  gulp.src("#{ src }/scripts/*.js")
    .pipe(gulp.dest("#{ target }/scripts"))
    .pipe(using(prefix: 'Output JS to'))

gulp.task 'images', ->
  gulp.src("#{ src }/images/**/*")
    .pipe(gulp.dest("#{ target }/images"))
    .pipe(using(prefix: 'Output Images to'))

gulp.task 'markup', ->
  gulp.src("#{ src }/index.jade")
      .pipe(jade(env: 'development'))
      .pipe(gulp.dest(target))
      .pipe(using(prefix: 'Output HTML to'))


gulp.task 'default', ['styles', 'scripts', 'markup', 'images'], ->
  gulp.watch("#{ src }/**/*.scss", ["styles"])
  gulp.watch("#{ src }/**/*.js", ["scripts"])
  gulp.watch("#{ src }/**/*.jade", ["markup"])
  gulp.watch("#{ src }/images/**/*", ["images"])

  browserSync.init(
    [
      "#{ target }/styles/*.css"
      "#{ target }/scripts/*.js"
      "#{ target }/images/**/*"
      "#{ target }/index.html"
    ]
    server: {
      baseDir: target
    }
  )






