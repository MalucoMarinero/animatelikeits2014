// RequestAnimFram polyfill

(function() {
  var lastTime = 0;
  var vendors = ['webkit', 'moz'];
  for(var x = 0; x < vendors.length && !window.requestAnimationFrame; ++x) {
    window.requestAnimationFrame = window[vendors[x]+'RequestAnimationFrame'];
    window.cancelAnimationFrame =
      window[vendors[x]+'CancelAnimationFrame'] || window[vendors[x]+'CancelRequestAnimationFrame'];
  }

  if (!window.requestAnimationFrame)
    window.requestAnimationFrame = function(callback, element) {
      var currTime = new Date().getTime();
      var timeToCall = Math.max(0, 16 - (currTime - lastTime));
      var id = window.setTimeout(function() { callback(currTime + timeToCall); },
        timeToCall);
      lastTime = currTime + timeToCall;
      return id;
    };

  if (!window.cancelAnimationFrame)
    window.cancelAnimationFrame = function(id) {
      clearTimeout(id);
    };
}());


var pfx = ["webkit", "moz", "MS", "o", ""];
function addPrefixedEventListener(element, type, callback) {
  for (var p = 0; p < pfx.length; p++) {
    if (!pfx[p]) type = type.toLowerCase();
    element.addEventListener(pfx[p]+type, callback, false);
  }
}

function removePrefixedEventListener(element, type, callback) {
  for (var p = 0; p < pfx.length; p++) {
    if (!pfx[p]) type = type.toLowerCase();
    element.removeEventListener(pfx[p]+type, callback, false);
  }
}

function jumpAnimationFrame(fn, frames) {
  frames = frames || 1;
  var iteration = 0;
  var jumper = function() {
    iteration += 1;
    if (iteration >= frames) {
      requestAnimationFrame(fn);
    } else {
      requestAnimationFrame(jumper);
    }
  }
  
  requestAnimationFrame(jumper);
}

function getQueryVariable(variable) {
   var query = window.location.search.substring(1);
   var vars = query.split("&");
   for (var i=0;i<vars.length;i++) {
           var pair = vars[i].split("=");
           if(pair[0] == variable){return pair[1];}
   }
   return(false);
}


// CONSTANTS
var KEY_RIGHT = 39;
var KEY_LEFT = 37;


function forEachNode(nodes, fn) {
  Array.prototype.forEach.call(nodes, fn);
}

function getLongestTransition(elem) {
  return Math.max.apply(null,
    getComputedStyle(elem)['transition-duration'].split(',').map(parseFloat)
  );
}

function getLongestDelay(elem) {
  return Math.max.apply(null,
    getComputedStyle(elem)['transition-delay'].split(',').map(parseFloat)
  );
}


document.addEventListener('DOMContentLoaded', function(){
  var currentSlide = parseInt(getQueryVariable('slide')) || 0,
      slides = document.body.querySelectorAll('.slides .slide');

  forEachNode(slides, function(el) {
    el.classList.add('-hidden');
  });

  slides[currentSlide].classList.remove('-hidden'); 

  document.body.classList.add('-on-slide-' + currentSlide);
  
  var timeTransition = function (elem, animName, animDir, onEnd, stagger) {
    var slide = elem;
    var transitionTime;
    var transitionEndTime;
    var delayUntil;
    slide.classList.add(animName + '-' + animDir);

    if (stagger) slide.classList.add(animName + '-stagger-' + animDir);

    var timeStagger = function (timer) {
      if (!timer) {
        requestAnimationFrame(timeStagger);
        return;
      }
      if (timer > delayUntil) {
        if (!slide.classList.contains(animName + '-' + animDir + '-active')) {
          slide.classList.add(animName + '-' + animDir + '-active');
        };
      } else {
        requestAnimationFrame(timeStagger);
      }
    };

    var transitionTimer = function (timer) {
      transitionTime = getLongestTransition(slide);
      if (transitionTime === 0) {
        if (onEnd) onEnd(slide);
        slide.classList.remove(animName + '-' + animDir);
        if (stagger) slide.classList.remove(animName + '-stagger-' + animDir);
        return;
      };

      if (stagger && !delayUntil) {
        delayUntil = timer + (getLongestDelay(slide) * 1000) * (stagger - 1);
      }

      var transEnd = function(ev) {
        if (ev.elapsedTime < transitionTime) return;
        slide.classList.remove(animName + '-' + animDir + '-active');
        slide.classList.remove(animName + '-' + animDir);
        if (stagger) slide.classList.remove(animName + '-stagger-' + animDir);
        if (onEnd) onEnd(slide);
        removePrefixedEventListener(slide, 'TransitionEnd', transEnd);
        return;
      };

      addPrefixedEventListener(slide, 'TransitionEnd', transEnd);

      if (delayUntil) {
        setTimeout(timeStagger, delayUntil - timer);
      } else {
        if (!slide.classList.contains(animName + '-' + animDir + '-active')) {
          transitionEndTime = timer + transitionTime * 1000;
          slide.classList.add(animName + '-' + animDir + '-active');
        };
      };
    };

    jumpAnimationFrame(transitionTimer);
  };

  var changeSlide = function (changeTo) {
    if (changeTo < 0 || changeTo >= slides.length) return;

    document.body.classList.remove('-on-slide-' + currentSlide);
    animName = changeTo > currentSlide ? '-anim-fwd' : '-anim-back';

    timeTransition(slides[currentSlide], animName, 'leave', function(elem) {
      if (elem != slides[currentSlide]) {
        elem.classList.add('-hidden'); 
      }
    });

    currentSlide = changeTo;

    timeTransition(slides[currentSlide], animName, 'enter');
    history.pushState({slide: currentSlide}, null, location.origin + '/?slide=' + currentSlide);

    document.body.classList.add('-on-slide-' + currentSlide);
    slides[currentSlide].classList.remove('-hidden'); 

  };

  // KEY CONTROLS
  document.body.addEventListener('keydown', function (event) {
    switch (event.keyCode) {
      case KEY_LEFT:
        event = document.createEvent('HTMLEvents');
        event.initEvent('click', true, false);
        $aPrev.dispatchEvent(event);
        break;
      case KEY_RIGHT:
        event = document.createEvent('HTMLEvents');
        event.initEvent('click', true, false);
        $aNext.dispatchEvent(event);
        break;
    }
  });

  $aNext = document.getElementById('next-slide');
  $aPrev = document.getElementById('prev-slide');
  $aNext.addEventListener('click', function(event) {
    changeSlide(currentSlide + 1);
  });
  $aPrev.addEventListener('click', function(event) {
    changeSlide(currentSlide - 1);
  });

  var touchStartX = null
  document.body.addEventListener('touchstart', function (event) {
    touchStartX = event.touches[0].clientX
  });

  document.body.addEventListener('touchmove', function (event) {
    event.preventDefault();
    if (!touchStartX) return;
    if (touchStartX + 100 <= event.touches[0].clientX) {
      touchStartX = null;

      event = document.createEvent('HTMLEvents');
      event.initEvent('click', true, false);
      $aPrev.dispatchEvent(event);
    } else if (touchStartX - 100 >= event.touches[0].clientX) {
      touchStartX = null;

      event = document.createEvent('HTMLEvents');
      event.initEvent('click', true, false);
      $aNext.dispatchEvent(event);
    }
  });

  var demoBox = document.getElementById('demo-box');
  var demoBoxModifiers = ['-blue', '-tall', '-chunky', '-faded'];

  demoBox.addEventListener('click', function() {
    for (var i = demoBoxModifiers.length - 1; i >= 0; i--) {
      var mod = demoBoxModifiers[i]
      Math.random() > 0.5 ? demoBox.classList.add(mod)
                          : demoBox.classList.remove(mod);
    }
  });



  var pageStack = document.getElementById('page-stack');
  var pageStackReset = document.getElementById('reset-page-stack');

  pageStack.addEventListener('click', function(event) {
    var elem = event.target;
    while (elem != pageStack && !elem.classList.contains('animated-page')) {
      elem = elem.parentNode;
    };
    if (elem != pageStack) {
      forEachNode(pageStack.querySelectorAll('.animated-page'), function(el) {
        el.classList.remove('-focussed');
        el.classList.add('-foreground');
      });
      elem.classList.add('-focussed');
      elem.classList.remove('-foreground');
    }
  });

  pageStack.addEventListener('click', function(event) {
    var elem = event.target;
    while (elem != pageStack && !elem.classList.contains('close')) {
      elem = elem.parentNode;
    };

    if (elem.classList.contains('close')) {
      while (!elem.classList.contains('animated-page')) {
        elem = elem.parentNode;
      };
      elem.classList.add('-closing');
    }
  });

  pageStackReset.addEventListener('click', function(event) {
    var pages = pageStack.querySelectorAll('.animated-page');

    forEachNode(pages, function(elem, ix) {
      elem.className = "animated-page -page-" + (ix+1);
      elem.className += " -foreground"
    });
  });

  forEachNode(document.body.querySelectorAll('.demo-accordion'), function(accordion) {
    var sections = accordion.querySelectorAll('li');
    var measured = accordion.classList.contains('-measured');

    var measuredStart = accordion.querySelector('.-measure .accordion-content');
    if (measuredStart) {
      var height = getComputedStyle(measuredStart)['height'];
      measuredStart.style.height = height;
    }

    accordion.addEventListener('click', function(event) {
      var elem = event.target;
      while (elem != accordion && elem.tagName != 'H4') {
        elem = elem.parentNode;
      };

      if (elem.tagName == 'H4') {
        while (elem.tagName != 'LI') {
          elem = elem.parentNode;
        };
        forEachNode(sections, function(el) {
          el.classList.remove('-active');
          el.classList.remove('-measure');
          if (measured) el.querySelector('.accordion-content').style.height = null;
        });
        elem.classList.add('-active');
        if (measured) {
          var content = elem.querySelector('.accordion-content');
          elem.classList.add('-measure');
          var height = getComputedStyle(content)['height'];
          elem.classList.remove('-measure');
          jumpAnimationFrame(function() {
            content.style.height = height;
          });
        }
      }
    });
  });



  var triggerStaggerDemo = document.getElementById('trigger-stagger-demo');
  var staggerLis = document.querySelectorAll('#stagger-demo li');

  triggerStaggerDemo.addEventListener('click', function() {
    forEachNode(staggerLis, function(el, ix) {
      el.classList.remove('-hidden');
      timeTransition(el, 'fade', 'enter', function() {
        timeTransition(el, 'fade', 'leave', function() {
          el.classList.add('-hidden');
          

        }, ix);
      }, ix);
    });
  });


});
